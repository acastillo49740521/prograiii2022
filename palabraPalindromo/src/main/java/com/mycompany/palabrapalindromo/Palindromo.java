
package com.mycompany.palabrapalindromo;

import java.util.Scanner;

public class Palindromo {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        int izq=0;
        int der;
        char [] palindromo;
        
        System.out.println("Ingrese una palabra: ");
        String palabra = entrada.nextLine();
        palabra=palabra.toLowerCase();
        palabra.replace(" ","");
        
        palindromo = palabra.toCharArray();        
        der=palindromo.length-1;
        
        while(izq<der){
            if(palindromo[izq]==palindromo[der]){
                der--;
                izq++;
            }else{
                System.out.print("La palabra no es un palindromo");
                break;
            }
        }
        
        if(izq==der){
            System.out.print("La palabra es un palindromo");
        }
        
        
        
        
        
    }
    
}
